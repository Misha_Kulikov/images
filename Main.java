import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static final String IN_FILE_TXT = "src\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\outFile.txt";
    private static final String PATH_TO_IMAGES = "src\\images\\";

    public static void main(String[] args) {
        extractImagesLinks();
        downloadImagesFiles();
    }

    /**
     * Скачивает найденное Изображения
     */
    private static void downloadImagesFiles() {
        try (BufferedReader imagesFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String imagesLink;
            int downloadedFilesCount = 0;
            try {
                
                while ((imagesLink = imagesFile.readLine()) != null) {
                    downloadUsingNIO(imagesLink, PATH_TO_IMAGES + String.valueOf(downloadedFilesCount)+".jpg" );
                    downloadedFilesCount++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Извлекает из вебстраниц ссылки на картинки  и сохраняет их в файл OUT_FILE_TXT
     */
    private static void extractImagesLinks() {
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {

            String webPageLink;
            while ((webPageLink = inFile.readLine()) != null) {

                URL webPageURL = new URL(webPageLink);

                String webPageHTML;
                /*
                 * openStream открывает поток ввода, связанный с веб-страницей
                 * InputStreamReader позволяет читать из InputStream символы, а не просто байты
                 * BufferedReader читает текст построчно
                 */
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(webPageURL.openStream()))) {

                    Stream<String> lines = bufferedReader.lines();
                    /*
                     * метод collect принимает на вход объект Collector, который
                     * определяет как нужно склеивать строки. Collectors.joining("\n")
                     * склеивает строки с помощью символа \n
                     */
                    webPageHTML = lines.collect(Collectors.joining("\n"));
                }

                /*
                 * \\("(?<=img(.{0,1000})src\\s?=\\s?\")https://[^>]*\\.jpg(?=\\s?\")")
                 * \\s - пробельный символ - табуляция, пробел и \n
                 * [^>] - все, кроме >, т.е. все кроме конца тега
                 */
                Pattern imagesLinkPattern = Pattern.compile("(?<=img(.{0,1000})src\\s?=\\s?\")https://[^>]*\\.jpg(?=\\s?\")");
                Matcher matcher = imagesLinkPattern.matcher(webPageHTML);
                int i = 0;
                while (matcher.find() && i < 2) {
                    outFile.write(matcher.group() + "\r\n");
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Скачивает файл из Интернета по ссылке imagesLink в файл на диске компьютера outputImagesFile
     *
     * @param imagesLink ссылка на файл в Интернете
     * @param outputImagesFile путь к файлу, в который будет скачано содержимое из Интернета
     * @throws IOException если произошла ошибка ввода-вывода
     */
    private static void downloadUsingNIO(String imagesLink, String outputImagesFile) throws IOException {
        URL musicURL = new URL(imagesLink);
        /*
         * ReadableByteChannel - канал, способный только читать данные из файла. Каналы отличаются тем,
         * что они изначально написаны создателями Java для работы в многопоточных системах
         * Если к одному источнику подключены два ReadableByteChannel, они не могут одновременно читать данные
         * Сначала прочитает один канал, а второй будет ждать пока источник освободится
         */
        ReadableByteChannel byteChannel = Channels.newChannel(musicURL.openStream());
        FileOutputStream fileStream = new FileOutputStream(outputImagesFile);

        /*
         * Метод getChannel позволяет преобразовать поток вывода в файловый канал
         */
        FileChannel fileChannel = fileStream.getChannel();

        /*
         * Метод transferFrom позволяет прочитать данные из byteChannel в файл,
         * к которому привязан fileChannel. 0 - стартовая позиция для чтения
         * Long.MAX_VALUE - количество прочитанных байт данных. MAX_VALUE - максимальное значение, которое
         * можно хранить в переменной типа long, означает что из источника мы прочитаем все байты
         */
        fileChannel.transferFrom(byteChannel, 0, Long.MAX_VALUE);
        fileStream.close();
        byteChannel.close();
    }
}